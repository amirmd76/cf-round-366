import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskA2 solver = new TaskA2();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskA2 {
        public static final String HATE = "I hate";
        public static final String LOVE = "I love";
        public static final String THAT = "that";
        public static final String IT = "it";

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            StringBuilder b = new StringBuilder("");
            for (int i = 0; i < n; ++i) {
                if (i % 2 == 0) {
                    b.append(HATE);
                } else {
                    b.append(LOVE);
                }
                b.append(' ');
                if (i < n - 1) {
                    b.append(THAT).append(' ');
                } else {
                    b.append(IT);
                }
            }
            out.println(b.toString());
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

    }
}

