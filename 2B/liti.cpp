//In the name of Allah
#include <bits/stdc++.h>
using namespace std;

int main() { 
	ios::sync_with_stdio(false); cin.tie(0);
	int n; 
	long long sum = 0; 
	cin >> n; 
	for( int i = 0 ; i < n ;i++ ) { 
		int x; cin >> x; 
		sum += x - 1; 
		cout << ( sum % 2 == 0 ? 2 : 1 ) << "\n" ; 
	}
}
