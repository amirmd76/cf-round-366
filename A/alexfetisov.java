import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.TreeSet;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskA_366 solver = new TaskA_366();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskA_366 {
        TreeSet<Integer> allNotifications = new TreeSet<Integer>();
        static TreeSet<Integer>[] appNotifications;
        int[] whichAppWasThat;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            int q = in.nextInt();

            appNotifications = new TreeSet[n];
            for (int i = 0; i < n; ++i) {
                appNotifications[i] = new TreeSet<Integer>();
            }
            whichAppWasThat = new int[2 * q];
            int id = 1;

            while (q-- > 0) {
                int type = in.nextInt();
                int x = in.nextInt();
                switch (type) {
                    case 1: {
                        --x;
                        allNotifications.add(id);
                        appNotifications[x].add(id);
                        whichAppWasThat[id] = x;
                        ++id;
                        break;
                    }
                    case 2: {
                        --x;
                        for (int i : appNotifications[x]) {
                            allNotifications.remove(i);
                        }
                        appNotifications[x].clear();
                        break;
                    }
                    case 3: {
                        while (!allNotifications.isEmpty()) {
                            int i = allNotifications.first();
                            if (i > x) break;
                            allNotifications.remove(i);
                            appNotifications[whichAppWasThat[i]].remove(i);
                        }
                        break;
                    }
                }
                out.println(allNotifications.size());
            }
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

    }
}

