#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 1e6 + 100;
set<int> s[maxn];
set<pii> se;
int main(){
	int n, q;
	scanf("%d %d", &n, &q);
	int nx = 0;
	while(q--){
		int type;
		scanf("%d", &type);
		if(type == 1){
			int x;
			scanf("%d", &x);
			-- x;
			s[x].insert(nx);
			se.insert({nx ++, x});
		}
		else if(type == 2){
			int x;
			scanf("%d", &x);
			-- x;
			rep(i, s[x])
				se.erase({i, x});
			s[x].clear();
		}
		else{
			int t;
			scanf("%d", &t);
			while(!se.empty() && se.begin()->x < t){
				pii p = *se.begin();
				se.erase(p);
				s[p.y].erase(p.x);
			}
		}
		printf("%d\n", (int)se.size());
	}
	return 0;
}
