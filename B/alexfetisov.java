import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskC_366 solver = new TaskC_366();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskC_366 {
        public static final long INF = Long.MAX_VALUE / 2;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            int start = in.nextInt() - 1;
            int end = in.nextInt() - 1;
            long[] x = Utils.readLongArray(in, n);
            long[] a = Utils.readLongArray(in, n);
            long[] b = Utils.readLongArray(in, n);
            long[] c = Utils.readLongArray(in, n);
            long[] d = Utils.readLongArray(in, n);

            long[][][] dp = new long[2][2][n + 2];
            long[][][] next = new long[2][2][n + 2];
            ArrayUtils.fill(dp, INF);
            ArrayUtils.fill(next, INF);
            dp[0][0][0] = 0;
            long result = INF;
            for (int i = 0; i < n; ++i) {

                for (int j = 0; j <= n; ++j) {
                    for (int k = 0; k < 2; ++k) {
                        for (int l = 0; l < 2; ++l) {
                            long cur = dp[k][l][j];
                            if (cur >= INF) continue;
                            if (i == start && k == 0) {
                                if (j > 0) {
                                    next[1][l][j - 1] = Math.min(next[1][l][j - 1], cur + x[i] + c[i]);
                                }
                                if (i == n - 1 && j == 0 && l != 0) {
                                    result = Math.min(result, cur + x[i] + c[i]);
                                }
                                next[1][l][j] = Math.min(next[1][l][j], cur - x[i] + d[i]);
                            }
                            if (i == end && l == 0) {
                                if (j > 0) {
                                    next[k][1][j - 1] = Math.min(next[k][1][j - 1], cur + x[i] + a[i]);
                                }
                                if (i == n - 1 && j == 0 && k != 0) {
                                    result = Math.min(result, cur + x[i] + a[i]);
                                }
                                next[k][1][j] = Math.min(next[k][1][j], cur - x[i] + b[i]);
                            }
                            if (i != start && i != end) {
                                if (j > 0 && j + k + l > 1) {
                                    next[k][l][j - 1] = Math.min(next[k][l][j - 1], cur + x[i] + a[i] + x[i] + c[i]);
                                }
                                if (i == n - 1 && j == 0 && k != 0 && l != 0) {
                                    result = Math.min(result, cur + x[i] + a[i] + x[i] + c[i]);
                                }

                                if (j != 0 || k != 0) {
                                    next[k][l][j] = Math.min(next[k][l][j], cur + a[i] + d[i]);
                                }
                                if (j != 0 || l != 0) {
                                    next[k][l][j] = Math.min(next[k][l][j], cur + b[i] + c[i]);
                                }

                                next[k][l][j + 1] = Math.min(next[k][l][j + 1], cur + b[i] - x[i] + d[i] - x[i]);
                            }
                        }
                    }
                }
                long[][][] temp = dp;
                dp = next;
                next = temp;
                ArrayUtils.fill(next, INF);
            }
            out.println(result);
        }

    }

    static class ArrayUtils {
        public static void fill(long[][] a, long value) {
            for (int i = 0; i < a.length; ++i) {
                Arrays.fill(a[i], value);
            }
        }

        public static void fill(long[][][] a, long value) {
            for (int i = 0; i < a.length; ++i) {
                fill(a[i], value);
            }
        }

    }

    static class Utils {
        public static long[] readLongArray(InputReader in, int n) {
            long[] a = new long[n];
            for (int i = 0; i < n; ++i) {
                a[i] = in.nextLong();
            }
            return a;
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

        public long nextLong() {
            return Long.parseLong(nextString());
        }

    }
}

