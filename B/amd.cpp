#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 5000 + 100;
const ll inf = 1LL << 60;
int a[maxn], b[maxn], c[maxn], d[maxn], x[maxn], n, s, e;
ll dp[2][maxn][2][2], ans = inf;
int main(){
	iOS;
	cin >> n >> s >> e;
	-- s, -- e;
	For(i,0,n)	cin >> x[i];
	For(i,0,n)	cin >> a[i];
	For(i,0,n)	cin >> b[i];
	For(i,0,n)	cin >> c[i];
	For(i,0,n)	cin >> d[i];
	fill(&dp[0][0][0][0], &dp[0][0][0][0] + 8 * maxn, inf);
	dp[1][0][0][0] = 0;
	For(i,0,n){
		bool I = i & 1;
		bool J = !I;
		For(j,0,n+1)	For(k,0,2)	For(l,0,2)	dp[I][j][k][l] = inf;
		For(j,0,n+1)	For(k,0,2)	For(l,0,2)	if(dp[J][j][k][l] != inf){
			ll val = dp[J][j][k][l];
			if(i != s && i != e){
				/* LL */{
					ll cur = 2LL * x[i] + a[i] + c[i];
					if(j + k + l > 1)	smin(dp[I][j-1][k][l], cur + val);
					if(i == n-1 && !j && k && l)	smin(ans, cur + val);
				}
				/* LR */{
					ll cur = a[i] + d[i];
					if(j or k)	smin(dp[I][j][k][l], cur + val);
				}
				/* RL */{
					ll cur = b[i] + c[i];
					if(j or l)	smin(dp[I][j][k][l], cur + val);
				}
				/* RR */{
					ll cur = -2LL * x[i] + b[i] + d[i];
					smin(dp[I][j+1][k][l], cur + val);
				}
			}
			else if(i == s && !k){
				/* L */{
					ll cur = x[i] + c[i];
					if(j)	smin(dp[I][j-1][1][l], cur + val);
					if(i == n-1 && !j && l)	smin(ans, cur + val);
				}
				/* R */{
					ll cur = -x[i] + d[i];
					smin(dp[I][j][1][l], cur + val);
				}
			}
			else if(i == e && !l){
				/* L */{
					ll cur = x[i] + a[i];
					if(j)	smin(dp[I][j-1][k][1], cur + val);
					if(i == n-1 && !j && k)	smin(ans, cur + val);

				}
				/* R */{
					ll cur = -x[i] + b[i];
					smin(dp[I][j][k][1], cur + val);
				}
			}
		}
	}
	cout << ans << endl;
	return 0;
}