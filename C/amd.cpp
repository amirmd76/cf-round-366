#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 1e5 + 100, mod = 1e9 + 7;
typedef pair<int, bool> pib;
vector<pib> lit[maxn], cl[maxn];
int V[maxn], U[maxn];
bool S[maxn];
vi adj[maxn];
int n, m;
bool mark[maxn];
pii ans = {1, 0};
inline void app(pii p){
	int x = (1LL * ans.x * p.x + 1LL * ans.y * p.y) % mod;
	int y = (1LL * ans.x * p.y + 1LL * ans.y * p.x) % mod;
	ans = {x, y};
}
bool block[maxn], isa[maxn];
vi pat;
vector<bool> sgn;
inline void dfs(int v){
	mark[v] = true;
	pat.pb(v);
	rep(e, adj[v]){
		if(block[e])	continue ;
		block[e] = true;
		sgn.pb(S[e]);
		int u = V[e] + U[e] - v;
		if(!mark[u])	dfs(u);
	}
}
int dp[maxn][2][2][2];
inline int MOD(int a){
	while(a >= mod)	a -= mod;
	return a;
}
inline void solve(){
	int sz = pat.size();
	//rep(i, pat)	error(i);
	For(i,0,sz + 2)
		For(j,0,2)	For(k,0,2)	For(l,0,2)	dp[i][j][k][l] = 0;
	dp[0][0][0][0] = 1;
	if(isa[pat[0]])
		dp[0][1][1][1] = 1;
	For(i,1,sz)
		For(b,0,2)
			For(e,0,2)
				For(t,0,2){
					int s = sgn[i-1];
					For(x,0,2){
						int p = e | x;
						int w = s ^ x;
						int B = b;
						if(i == 1)	B = p;
						int T = (2 + t + w + p - e) & 1;
						dp[i][B][w][T] = MOD(dp[i][B][w][T] + dp[i-1][b][e][t]);
					}
				}
	int o = sz-1;
	if((int)sgn.size() == sz){
		++ o;
		int s = sgn.back();
		For(b,0,2)
			For(e,0,2)
				For(t,0,2)
					For(x,0,2){
						int w = e | x;
						int B = b | (s ^ x);
						int T = (4 + t + w - e + B - b) & 1;
						dp[o][B][w][T] = MOD(dp[o][B][w][T] + dp[o-1][b][e][t]);
					}
	}
	else if(isa[pat.back()]){
		++ o;
		For(b,0,2)
			For(e,0,2)
				For(t,0,2)	For(x,0,2){
					int w = e | x;
					int T = (2 + t + w - e) & 1;
					dp[o][b][w][T] = MOD(dp[o][b][w][T] + dp[o-1][b][e][t]);
				}
	}
	int ans[2];
	ans[0] = ans[1] = 0;
	For(b,0,2)
		For(e,0,2)
			For(t,0,2)	ans[t] = MOD(ans[t] + dp[o][b][e][t]);
	app({ans[0], ans[1]});
}
int main(){
	scanf("%d %d", &n, &m);
	For(i,0,n){
		int k;
		scanf("%d", &k);
		while(k--){
			int a;
			scanf("%d", &a);
			bool s = a > 0;
			a = abs(a);
			-- a;
			cl[i].pb({a, s});
			lit[a].pb({i, s});
		}
	}
	int t = 1;
	For(i,0,m){
		if(lit[i].empty())	
			t = (2LL * t) % mod;
		else if((int)lit[i].size() == 2){
			bool s = lit[i][0].y ^ lit[i][1].y;
			int v = lit[i][0].x, u = lit[i][1].x;
			if(v == u){
				isa[v] = true;
				if(s){
					app({0, 2});
					cl[v].clear();
				}
				else
					cl[v].PB;
			}
			else{
				adj[v].pb(i);
				adj[u].pb(i);
				V[i] = v, U[i] = u, S[i] = s;
			}
		}
		else
			isa[lit[i][0].x] = true;
	}
	For(i,0,n){
		if(mark[i])	continue ;
		if(cl[i].empty()){
			mark[i] = true;
			continue ;
		}
		if(adj[i].empty()){
			mark[i] = true;
			if(cl[i].size() == 1)
				app({1, 1});
			else
				app({1, 3});
			continue ;
		}
		if(adj[i].size() == 1){
			pat.clear();
			sgn.clear();
			dfs(i);
			solve();
		}
	}
	For(i,0,n)	if(!mark[i]){
		pat.clear();
		sgn.clear();
		dfs(i);
		solve();
	}
	int ans = ::ans.y;
	ans = (1LL * ans * t) % mod;
	printf("%d\n", ans);
	return 0;
}
