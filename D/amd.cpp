#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
template <class FlowT> class MaxFlow{
	public:
		static const int maxn = 2e5 + 100, maxm = 5e5 + 100;
		static const FlowT FlowEPS = FlowT(1e-8), FlowINF = FlowT(1 << 29);
		int to[maxm * 2], prv[maxm * 2], hed[maxn], dis[maxn], pos[maxn];
		FlowT cap[maxm * 2];
		int n, m;
		inline void init(int N){n = N, m = 0;memset(hed, -1, n * sizeof hed[0]);}
	private:
		inline void add_single_edge(int v, int u, FlowT c){
			to[m] = u, prv[m] = hed[v], cap[m] = c, hed[v] = m ++;
		}
	public:
		inline void add_edge(int v, int u, FlowT c, FlowT d = 0){
			add_single_edge(v, u, c);
			add_single_edge(u, v, d);
		}
		inline bool bfs(int source, int sink){
			static int qu[maxn], head, tail;
			head = tail = 0;
			memset(dis, -1, n * sizeof dis[0]);
			dis[source] = 0;
			qu[tail ++] = source;
			while(head < tail){
				int v = qu[head ++];
				for(int e = hed[v]; e + 1; e = prv[e])
					if(cap[e] > FlowEPS && dis[to[e]] == -1)
						dis[to[e]] = dis[v] + 1, qu[tail ++] = to[e];
				if(dis[sink] + 1)	break ;
			}
			return dis[sink] + 1;
		}
		inline FlowT dfs(int v, int sink, FlowT cur = FlowINF){
			if(v == sink)	return cur;
			FlowT ans = 0;
			for(int &e = pos[v]; e + 1; e = prv[e])
				if(cap[e] > FlowEPS && dis[to[e]] == dis[v] + 1){
					FlowT tmp = dfs(to[e], sink, min(cur, cap[e]));
					cur -= tmp;
					ans += tmp;
					cap[e] -= tmp;
					cap[e ^ 1] += tmp;
					if(cur <= FlowEPS/2)	break ;
				}
			return ans;
		}
		inline FlowT flow(int source, int sink){
			FlowT ans = 0;
			while(bfs(source, sink)){
				memcpy(pos, hed, n * sizeof hed[0]);
				ans += dfs(source, sink);
			}
			return ans;
		}
};
MaxFlow<int> F, G;
const int maxn = 1e5 + 100;
vi xs, ys;
int x[maxn], y[maxn];
inline int MPX(int x){
	int i = lower_bound(all(xs), x) - xs.begin();
	if(i == (int)xs.size() or xs[i] != x)	return -1;
	return i;
}
inline int MPY(int y){
	int i = lower_bound(all(ys), y) - ys.begin();
	if(i == (int)ys.size() or ys[i] != y)	return -1;
	return i;
}
int cntx[maxn], cnty[maxn], cx[maxn], cy[maxn], idx[maxn], idy[maxn], Lx[maxn], Rx[maxn], Ly[maxn], Ry[maxn], id[maxn];
int no_warn;
int main(){
	int n, m;
	int R, B;
	no_warn = scanf("%d %d", &n, &m);
	no_warn = scanf("%d %d", &R, &B);
	For(i,0,n){
		no_warn = scanf("%d %d", x + i, y + i);
		xs.pb(x[i]);
		ys.pb(y[i]);
	}
	sort(all(xs));
	sort(all(ys));
	xs.resize(unique(all(xs)) - xs.begin());
	ys.resize(unique(all(ys)) - ys.begin());
	For(i,0,n){
		x[i] = MPX(x[i]);
		y[i] = MPY(y[i]);
		++ cntx[x[i]], ++ cx[x[i]];
		++ cnty[y[i]], ++ cy[y[i]];
	}
	int X = xs.size(), Y = ys.size();
	while(m--){
		int t, i, d;
		no_warn = scanf("%d %d %d", &t, &i, &d);
		if(t == 1){
			i = MPX(i);
			if(i == -1)	continue ;
			smin(cx[i], d);
		}
		else{
			i = MPY(i);
			if(i == -1)	continue ;
			smin(cy[i], d);
		}
	}
	int s = X + Y,
		t = s + 1,
		S = t + 1,
		T = S + 1;
	F.init(T + 1);
	F.add_edge(t, s, n + 10);
	int tot = 0;
	For(i,0,X){
		int deg = cntx[i];
		int d = cx[i];
		Lx[i] = (deg - d + 1) / 2;
		Rx[i] = (deg + d + 0) / 2;
		if(Lx[i] > Rx[i]){
			cout << -1 << endl;
			return 0;
		}
		idx[i] = F.m;
		F.add_edge(s, i, Rx[i] - Lx[i]);
		if(Lx[i]){
			F.add_edge(S, i, Lx[i]);
			F.add_edge(s, T, Lx[i]);
			tot += Lx[i];
		}
	}
	For(i,0,n){
		id[i] = F.m;
		F.add_edge(x[i], X + y[i], 1);
	}
	For(i,0,Y){
		int deg = cnty[i];
		int d = cy[i];
		Ly[i] = (deg - d + 1) / 2;
		Ry[i] = (deg + d + 0) / 2;
		if(Ly[i] > Ry[i]){
			cout << -1 << endl;
			return 0;
		}
		idy[i] = F.m;
		F.add_edge(X + i, t, Ry[i] - Ly[i]);
		if(Ly[i]){
			F.add_edge(S, t, Ly[i]);
			F.add_edge(X + i, T, Ly[i]);
			tot += Ly[i];
		}
	}
	int f = F.flow(S, T);
	if(f != tot){
		puts("-1");
		return 0;
	}
	G.init(t + 1);
	For(i,0,X)
		G.add_edge(s, i, F.cap[idx[i]], Rx[i] - Lx[i] - F.cap[idx[i]]);
	For(i,0,n){
		int C = F.cap[id[i]];
		int D = 1 - C;
		id[i] = G.m;
		G.add_edge(x[i], X + y[i], C, D);
	}
	For(i,0,Y)
		G.add_edge(X + i, t, F.cap[idy[i]], Ry[i] - Ly[i] - F.cap[idy[i]]);
	G.flow(s, t);
	string g = "rb";
	if(R > B)
		swap(R, B), swap(g[0], g[1]);
	string ans;
	ll cost = 0;
	For(i,0,n){
		int c = G.cap[id[i]];
		cost += (c? B: R);
		ans += g[c];
	}
	cout << cost << '\n';
	puts(ans.c_str());
	return 0;
}