#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 1e5 + 10, maxl = 19;
const ll inf = 1LL << 60;
const double eps = 1e-12;
struct frac{
	ll x, y;
	frac(){x = 0, y = 1;}
	frac(ll X, ll Y){
		x = X, y = Y;
		ll g = __gcd(abs(x), abs(y));
		x /= g, y /= g;
		if(y < 0)	x = -x, y = -y;
	}
	inline void relax(){
		ll g = __gcd(abs(x), abs(y));
		x /= g, y /= g;

	}
	frac(ll X){x = X, y = 1;}
	inline ostream& operator << (ostream &cout){
		cout << (double)x/(double)y;
		return cout;
	}
	inline const double val() const{
		return (double)x/(double)y;
	}
	inline void operator = (const frac &r){
		x = r.x, y = r.y;
	}
	inline bool operator < (const frac &r)	const{
		return val() < r.val();
	}
	inline bool operator > (const frac &r)	const{
		return val() > r.val();
	}
	inline bool operator <= (const frac &r)	const{
		return val() < r.val() + eps;
	}
	inline bool operator >= (const frac &r)	const{
		return val() + eps > r.val();
	}
	inline frac operator + (const frac &r) const{
		frac ans;
		if(y == r.y){
			ans.x = x + r.x;
			ans.y = y;
		}
		else{
			ans.y = y * r.y;
			ans.x = x * r.y + r.x * y;
		}
		if(!ans.x)	ans.y = 1;
		if(ans.y < 0)	ans.x = -ans.x, ans.y = -ans.y;
		return ans;
	}
	inline frac operator - (const frac &r) const{
		frac ans;
		if(y == r.y){
			ans.x = x - r.x;
			ans.y = y;
			return ans;
		}
		else{
			ans.y = y * r.y;
			ans.x = x * r.y - r.x * y;
		}
		if(!ans.x)	ans.y = 1;
		if(ans.y < 0)	ans.x = -ans.x, ans.y = -ans.y;
		return ans;
	}
	inline frac operator * (const frac &r) const{
		frac ans;
		ans.y = y * r.y;
		ans.x = x * r.x ;
		ll g = __gcd(abs(ans.x), abs(ans.y));
		ans.x /= g, ans.y /= g;
		if(!ans.x)	ans.y = 1;
		if(ans.y < 0)	ans.x = -ans.x, ans.y = -ans.y;
		return ans;
	}
	inline frac operator / (const frac &r) const{
		frac ans;
		ans.y = y * r.x;
		ans.x = x * r.y ;
		if(!ans.x)	ans.y = 1;
		if(ans.y < 0)	ans.x = -ans.x, ans.y = -ans.y;
		return ans;
	}

	inline frac operator + (const ll &r) const{
		return (*this) + frac(r);
	}
	inline frac operator - (const ll &r) const{
		return (*this) - frac(r);
	}
	inline frac operator * (const ll &r) const{
		return (*this) * frac(r);
	}
	inline frac operator / (const ll &r) const{
		return (*this) / frac(r);
	}

};
int st[maxn], ft[maxn], par[maxn][maxl], h[maxn], tim, sz[maxn], fst[maxn], stm[maxn], lst[maxn];
int BIG[maxn];
vi adj[maxn];
int n, m;
struct CAR{
	int v, u, c;
	frac t;
}a[maxn];
inline void DFS(int v = 0, int p = -1){
	sz[v] = 1;
	rep(u, adj[v])	if(u - p){
		DFS(u, v);
		sz[v] += sz[u];
	}
}
inline void dfs(int v = 0, int p = -1, bool big = false){
	BIG[v] = v;
	fst[v] = (big? fst[p]: v);
	stm[tim] = v;
	st[v] = tim ++;
	par[v][0] = p;
	if(~p)	h[v] = h[p] + 1;
	For(i,1,maxl)	if(~par[v][i-1])
		par[v][i] = par[par[v][i-1]][i-1];
	int mx = 0, I = -1;
	For(i,0,adj[v].size())	if(adj[v][i] - p && mx < sz[adj[v][i]])
		mx = sz[adj[v][i]], I = i, BIG[v] = adj[v][i];
	if(~I)
		rotate(adj[v].begin(), adj[v].begin() + I, adj[v].end());
	For(i,0,adj[v].size())	if(adj[v][i] - p)
		dfs(adj[v][i], v, !i);
	ft[v] = tim;
	smax(lst[fst[v]],  st[v]);
}
inline bool anc(int v, int u){// u is ancestor of v
	return st[u] <= st[v] && ft[v] <= ft[u];
}
inline int lca(int v, int u){
	if(h[v] < h[u])	swap(v, u);
	rof(i,maxl-1,-1)	if(~par[v][i] && h[par[v][i]] >= h[u])
		v = par[v][i];
	if(v == u)	return v;
	rof(i,maxl-1,-1)	if(par[v][i] - par[u][i])
		v = par[v][i], u = par[u][i];
	return par[v][0];
}
typedef pair<frac, frac> pd;
typedef pair<pii, int> piii;
typedef pair<piii, pd> car;
vector<car> cars[maxn];
inline int sign(frac x){
	if(!x.x)	return 0;
	if(x.x > 0)	return 1;
	return -1;
}
inline void add(int v, int u, int i, bool LA = true, bool rev = false){ // u is ancestor of v
	int V = v;
	while(fst[v] != fst[u]){
		int l = v;
		int r = par[fst[v]][0];
		frac b = frac(h[V] - h[l])/frac(a[i].c) + a[i].t;
		frac e = frac(h[V] - h[r])/frac(a[i].c) + a[i].t;
		if(!rev)	cars[fst[v]].pb({{{h[l], h[r]}, i}, {b, e}});
		else		cars[fst[v]].pb({{{h[r], h[l]}, i}, {e, b}});
		v = par[fst[v]][0];
	}
	if(!LA)	return ;
	int l = v;
	int r = u;
	frac b = frac(h[V] - h[l])/frac(a[i].c) + a[i].t;
	frac e = frac(h[V] - h[r])/frac(a[i].c) + a[i].t;
	if(!rev)	cars[fst[v]].pb({{{h[l], h[r]}, i}, {b, e}});
	else		cars[fst[v]].pb({{{h[r], h[l]}, i}, {e, b}});
}
frac ans(2e5);
int I = -1, J = -1, ID;
frac TIME;

inline frac pos(const int &i){
	frac dis (cars[ID][i].x.x.y - cars[ID][i].x.x.x);
	frac T = cars[ID][i].y.y - cars[ID][i].y.x;
	if(!sign(T))
		return cars[ID][i].x.x.x;
	frac V (sign(dis) * a[cars[ID][i].x.y].c);
	frac ans =  V * (TIME - cars[ID][i].y.x) + cars[ID][i].x.x.x ;
	return ans;
}
struct cmp{
	inline bool operator ()(const int &i, const int &j) const{
		frac a = pos(i), b = pos(j);
		if(a < b or a > b)	return a < b;
		return i < j;
	}
};
inline void check(int i, int j){
	frac A = pos(i), B = pos(j);
	if(A < B)	swap(A, B), swap(i, j);
	if(A <= B && A >= B){
		if(ans > TIME)
			ans = TIME, I = cars[ID][i].x.y, J = cars[ID][j].x.y;
		return ;
	}
	frac dis (cars[ID][i].x.x.y - cars[ID][i].x.x.x);
	frac T = cars[ID][i].y.y - cars[ID][i].y.x;
	if(!sign(T)){
		return ;
	}
	frac VI (sign(dis) * a[cars[ID][i].x.y].c);

	dis = frac(cars[ID][j].x.x.y - cars[ID][j].x.x.x);
	T = cars[ID][j].y.y - cars[ID][j].y.x;
	if(!sign(T)){
		return ;
	}
	frac VJ (sign(dis) * a[cars[ID][j].x.y].c);

	if(sign(VI) > 0  && VJ <= VI){
		return ;
	}
	if(sign(VJ) < 0 && VI >= VJ){
		return ;
	}
	frac t = TIME + (A-B)/(VJ-VI);
	frac e = min(cars[ID][i].y.y, cars[ID][j].y.y);
	if(t > min(cars[ID][i].y.y, cars[ID][j].y.y))	return ;
	if(t < max(cars[ID][i].y.x, cars[ID][j].y.x))	return ;
	if(ans > t)
		ans = t, I = cars[ID][i].x.y, J = cars[ID][j].x.y;
}
inline void solve(int id){
	ID = id;
	typedef pair<frac, pii> event;
	set<event> se;
	set<int, cmp> pp;
	For(i,0,cars[id].size()){
		se.insert({cars[id][i].y.x, {0, i}});
		se.insert({cars[id][i].y.y, {1, i}});
	}
	rep(e, se){
		if(e.x >= ans)	return ;
		TIME = e.x;
		int i = e.y.y;
		if(!e.y.x){
			pp.insert(i);
			auto it = pp.find(i);
			auto it2 = it;
			if(it != pp.begin()){
				-- it;
				check(*it, i);
			}
			++ it2;
			if(it2 != pp.end()){
				check(i, *it2);
			}
		}
		else{
			auto it = pp.find(i);
			assert(it != pp.end());
			auto it2 = it;
			++ it2;
			if(it != pp.begin() && it2 != pp.end()){
				-- it;
				check(*it, *it2);
			}
			pp.erase(i);
		}
	}
}
int no_warn;
int main(){
	memset(par, -1, sizeof par);
	no_warn = scanf("%d %d", &n, &m);
	For(i,1,n){
		int v, u;
		no_warn = scanf("%d %d", &v, &u);
		-- v, -- u;;
		adj[v].pb(u), adj[u].pb(v);
	}
	DFS();
	dfs();
	For(i,0,m){
		int t;
		no_warn = scanf("%d %d %d %d", &t, &a[i].c, &a[i].v, &a[i].u);
		a[i].t = frac(t);
		int &v = a[i].v, &u = a[i].u;
		-- v, -- u;
		int x = lca(v, u);
		bool V = false, U = false;
		if(v == u){
			add(v, x, i);
			continue;
		}
		if(x != v && (x == u or anc(v, BIG[x])))	V = true;
		else	U = true;
		if(x != v){
			add(v, x, i, V);
		}
		if(x != u){
			frac T = frac(h[v] + h[u] - 2*h[x])/frac(a[i].c) + a[i].t, tt(a[i].t);
			a[i].c *= -1;
			a[i].t = T;
			add(u, x, i, U, true);
			a[i].c *= -1;
			a[i].t = tt;
		}
	}
	For(i,0,n)	if(fst[i] == i)	solve(i);
	if(ans >= frac(2e5)){
		cout << -1 << endl;
		return 0;
	}
	cout << fixed << setprecision(30) << (double)ans.x/(double)ans.y << endl;
	return 0;
}
